export class UserModel {

    constructor(
        public email: string,
        public image: string,
        public nome: string,
        public roles: string,
        public token: string,
        public userName: string,
    ) {

    }
}
